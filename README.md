# EvenOdd The Game

Simple demo for EvenOdd multiplayer game.

## Prerequesties

This app tested on Node.js with next versions:

```sh
node -v && npm -v
v8.1.4
3.8.2
```

## Preparation

### NPM Dependencies

```sh
yarn # install all dependencies
```

### Firebase Account

Then you need to duplicate `./.env.sample` to `./.env` and fill [Firebase](https://console.firebase.google.com/) settings with your own (e.g. `FIREBASE.apiKey` && `FIREBASE.messagingSenderId`).

In Console, allow Anonimous Authentication and set database `.read` and `.write` rules to `true` value.

## Usage

```sh
yarn serve # ...then open http://localhost:8100 in your favorite 
           # web broswer or mobile device
```
