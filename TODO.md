# TODO

[x] Validate same names in `join-game` page.
[x] Check if 'create'/'join' game states available.
[x] Make views:
  [x] Create Game
  [x] Join Game
  [x] In Game
[x] Migrate to Ngrx
[x] Migrate to RxJS 
[x] Display turn number
[x] Display turn result after turnCommit()
[x] Write game end logic
[x] Move all files from 'app' to project root
[x] Write consistent README.md
[x] Display 'you win'/'you loose'

## Refactor

[x] gameReducer - move game rules logic to gameRules singletone used by GameEffects
[x] Move static library methods from GameService to GameHelpers helper library
[x] create actions/{game|player}Actions to move some dispatching code from GameService

## Fix

[x] 'Create' button showing for 'joinable' game few seconds after app startup
[ ] Non-instant data updates (happens sometimes in Webkit-based browsers)
[x] Game recreation (probably must unsubscribe from pipes inside components)
[x] Last turn for first player (game creator)

## Nice to do

[ ] Migrate to ImmutableJS
[ ] use LocalStorage to save current game and player id (in GameService)
