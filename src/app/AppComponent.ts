/**
 * AppComponent is a certain app rendering component
 */
import { Component } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Store } from '@ngrx/store';
import { Platform } from 'ionic-angular';

import { GameService } from './services/GameService';

import { StartPage } from '../pages/StartPage/StartPage';

@Component({
  templateUrl: 'app.html',
})
export class AppComponent {
  public rootPage: {} = StartPage;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    gs: GameService,
    store: Store<{}>,
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      statusBar.styleDefault();
      splashScreen.hide();

      gs.checkGame();
    });
  }
}
