/**
 * gameActions provide actions for gameReducer
 */

import { Action } from '@ngrx/store';

import { IGame } from './../interfaces/IGame';
import { IPlayer } from './../interfaces/IPlayer';
import { ITurn } from './../interfaces/ITurn';

export const GAME_ACTIONS: {[key: string]: string} = {
  CREATE_GAME  : 'CREATE_GAME',
  EXIT_GAME    : 'EXIT_GAME',
  GAME_LOADED  : 'GAME_LOADED',
  GAME_UPDATED : 'GAME_UPDATED',
  INITIATE_GAME: 'INITIATE_GAME',
  JOIN_GAME    : 'JOIN_GAME',
  PLAYER_TURN  : 'PLAYER_TURN',
};

// Create new game
export function createGame(maxPlayers: number, username: string): Action {
  return {
    payload: { maxPlayers, username },
    type: GAME_ACTIONS.CREATE_GAME,
  };
}

// Allow create new game.
export function allowCreateGame(): Action {
  return {
    type: GAME_ACTIONS.INITIATE_GAME,
  };
}

// Join existing game
export function joinGame(username: string): Action {
  return {
    payload: { username },
    type: GAME_ACTIONS.JOIN_GAME,
  };
}

// Allow game to join
export function allowJoinGame(game: IGame): Action {
  return {
    payload: { game },
    type: GAME_ACTIONS.GAME_LOADED,
  };
}

// Make player turn
export function playerTurn(turn: ITurn, player: IPlayer): Action {
  return {
    payload: { turn, player },
    type: GAME_ACTIONS.PLAYER_TURN,
  };
}

// Sync game with loaded data
export function updateGame(game: IGame): Action {
  return {
    payload: { game },
    type: GAME_ACTIONS.GAME_UPDATED,
  };
}

// Exit game
export function gameExit(): Action {
  return {
    type: GAME_ACTIONS.EXIT_GAME,
  };
}
