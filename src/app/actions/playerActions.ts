/**
 * playerActions provides set of actions for playerReducer
 */

import { Action } from '@ngrx/store';

import { IGame } from './../interfaces/IGame';
import { IPlayer } from './../interfaces/IPlayer';

export const PLAYER_ACTIONS: {[key: string]: string} = {
  CREATE           : 'CREATE_PLAYER',
  SYNC_PLAYER_SCORE: 'SYNC_PLAYER_SCORE',
};

export function createPlayer(player): Action {
  return {
    payload: { player },
    type: PLAYER_ACTIONS.CREATE,
  };
}

export function syncPlayerScore(game: IGame, player: IPlayer): Action {
  return {
    payload: { game, player },
    type: PLAYER_ACTIONS.SYNC_PLAYER_SCORE,
  };
}
