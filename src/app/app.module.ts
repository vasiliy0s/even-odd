/**
 * Application certain module with configuration
 */

import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { Game } from '../components/Game/Game';
import { PlayersBoard } from '../components/PlayersBoard/PlayersBoard';
import { CreateGamePage } from '../pages/CreateGamePage/CreateGamePage';
import { IngamePage } from '../pages/IngamePage/IngamePage';
import { JoinGamePage } from '../pages/JoinGamePage/JoinGamePage';
import { StartPage } from '../pages/StartPage/StartPage';
import { GameEnd } from './../components/GameEnd/GameEnd';
import { IngameAwait } from './../components/IngameAwait/IngameAwait';
import { PlayerTurn } from './../components/PlayerTurn/PlayerTurn';
import { AppComponent } from './AppComponent';

import { GameEffects } from './effects/GameEffects';
import { gameReducer } from './reducers/gameReducer';
import { playerReducer } from './reducers/playerReducer';
import { GameService } from './services/GameService';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FIREBASE_CONFIG } from '../config/FIREBASE_CONFIG';

// tslint:disable no-stateless-class
// tslint:disable export-name
@NgModule({
  bootstrap: [IonicApp],
  declarations: [
    AppComponent,
    StartPage,
    CreateGamePage,
    JoinGamePage,
    IngamePage,
    IngameAwait,
    Game,
    GameEnd,
    PlayersBoard,
    PlayerTurn,
  ],
  entryComponents: [
    AppComponent,
    StartPage,
    CreateGamePage,
    JoinGamePage,
    IngamePage,
  ],
  imports: [
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireDatabaseModule,
    EffectsModule.runAfterBootstrap(GameEffects),
    FormsModule,
    BrowserModule,
    IonicModule.forRoot(AppComponent),
    StoreModule.provideStore({
      game: gameReducer,
      player: playerReducer,
    }),
  ],
  providers: [
    GameService,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
  ],
})
export class AppModule {}
