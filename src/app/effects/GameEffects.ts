/**
 * GameEffect is hooks after gameReducer applying
 */

import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import * as firebase from 'firebase';
import { Observable, Subscription } from 'rxjs';
// tslint:disable no-import-side-effect
import 'rxjs/add/operator/catch';

import { GAME_ACTIONS } from './../actions/gameActions';
import { IGame } from './../interfaces/IGame';
import { IPlayer } from './../interfaces/IPlayer';
import { GameService } from './../services/GameService';

@Injectable()
export class GameEffects {
  // createGame$
  // Save created game to database
  @Effect({ dispatch: false }) public createGame$: {} | Subscription = this.actions$
    .ofType(GAME_ACTIONS.CREATE_GAME)
    .switchMap(() =>
      Observable.from(this.game.first())
        .map((game: IGame) => {
          // console.log('createGame$ effect applyed, game:', game);
          const newPostRef: firebase.database.Reference = this.gamesList.$ref.ref.push();
          game.id = newPostRef.key;

          return Observable.fromPromise(newPostRef.set(game))
            .subscribe(() => this.gs.onGameCreated());
        })
        .catch(console.error.bind(console, 'createGame$ effect error:')),
    );

  // joinGame$
  @Effect({ dispatch: false }) public joinGame$: {} | Subscription = this.actions$
    .ofType(GAME_ACTIONS.JOIN_GAME)
    .switchMap(() =>
      Observable.from(this.game.first())
        .map((game: IGame) => {
          // console.log('joinGame$ effect applyed, game:', game);
          return Observable.fromPromise(
            this.gamesList.$ref.ref.child(game.id).update(game),
          )
            .subscribe(() => this.gs.onGameJoined());
        })
        .catch(console.error.bind(console, 'joinGame$ effect error:')),
    );

  // playerTurn$
  @Effect({ dispatch: false }) public playerTurn$: {} | Subscription = this.actions$
    .ofType(GAME_ACTIONS.PLAYER_TURN)
    .switchMap(action =>
      Observable.from(this.game.first())
        .map((game: IGame) => {
          const { payload } = action;
          const player: IPlayer = payload.player;
          // console.log('playerTurn$ effect applyed, game:', game);
          return Observable.fromPromise(
            this.gamesList.$ref.ref.child(game.id).update(game),
          )
            .subscribe(() => this.gs.afterPlayerTurn(game, player));
        })
        .catch(console.error.bind(console, 'playerTurn$ effect error:')),
    );

  // gameExit$
  @Effect({ dispatch: false }) public gameExit$: {} | Subscription = this.actions$
    .ofType(GAME_ACTIONS.EXIT_GAME)
    .switchMap(() =>
      Observable.from(this.game.first())
        .map(() => this.gs.onGameExit())
        .catch(console.error.bind(console, 'gameExit$ effect error:')),
    );

  private gamesList: FirebaseListObservable<IGame[]>;
  private game: Observable<IGame>;

  constructor(
    private actions$: Actions,
    db: AngularFireDatabase,
    store: Store<{}>,
    private gs: GameService,
  ) {
    this.gamesList = db.list('/games');
    this.game = store.select('game');
  }
}
