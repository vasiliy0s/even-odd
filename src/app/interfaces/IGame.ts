/**
 * Game desciption interface
 */

import { IPlayer } from './IPlayer';
import { ITurn } from './ITurn';

export const GAME_STATUSES: {[key: string]: string} = {
  GAME_AWAIT  : 'GAME_AWAIT',
  GAME_ENDED  : 'GAME_ENDED',
  GAME_INITIAL: 'GAME_INITIAL',
  GAME_STARTED: 'GAME_STARTED',
};

export const JOINABLE_GAME_STATUSES: string[] = [
  GAME_STATUSES.GAME_AWAIT,
];

export interface IGame {
  $key?: string; // firebase database ID key
  id: string; // database ID of the game
  players: IPlayer[];
  rounds: ITurn[][]; // all game turn results
  maxPlayers: number;
  status: string;
  winner: IPlayer;
}
