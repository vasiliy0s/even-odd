/**
 * IPlayer interface presence current player data
 */
export interface IPlayer {
  username: string;
  order: number;
  score: number;
}
