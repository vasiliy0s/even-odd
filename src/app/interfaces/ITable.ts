/**
 * ITable is structure to sort players over reduce
 */

import { IPlayer } from './IPlayer';

export interface ITable {
  winner: IPlayer;
  score: number;
  count: number;
}
