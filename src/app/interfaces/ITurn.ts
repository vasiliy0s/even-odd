/**
 * ITurn interface presence player's turn results
 */

export interface ITurn {
  answerIsEven: boolean; // player' turn answer
  complete: boolean; // is turn complete
  madeNumber: number; // player' turn number result
  result: boolean; // is player give correct answer or not
  username: string;
}
