/**
 * gameReducer changes the IGame store component with Ngrx
 */

import { Action } from '@ngrx/store';

import { GAME_STATUSES, IGame } from '../interfaces/IGame';
import { IPlayer } from '../interfaces/IPlayer';
import { makeRoundTurnsQueueFor } from '../services/gameHelpers';
import { createGame, joinGame, makePlayerTurn } from '../services/gameRules';
import { GAME_ACTIONS } from './../actions/gameActions';
import { ITurn } from './../interfaces/ITurn';

// Initial game state (no available game, blank scaffold)
const gameInitial: IGame = {
  id: null,
  maxPlayers: 0,
  players: [],
  rounds: [],
  status: GAME_STATUSES.GAME_INITIAL,
  winner: null,
};

export function gameReducer(state: IGame = gameInitial, action: Action): IGame {
  const { payload } = action;

  switch (action.type) {
    // Just initiate empty game data.
    case GAME_ACTIONS.INITIATE_GAME:
      return { ...state };

    // Give available loaded games.
    case GAME_ACTIONS.GAME_LOADED:
      return { ...payload.game };

    // It creates game with initial data.
    case GAME_ACTIONS.CREATE_GAME:
      return createGame(payload.maxPlayers, payload.username);

    // Join game and, if allowed, run
    case GAME_ACTIONS.JOIN_GAME:
      return joinGame(state, payload.username);

    case GAME_ACTIONS.EXIT_GAME:
      return { ...gameInitial };

    // Game status updated from server.
    case GAME_ACTIONS.GAME_UPDATED:
      return { ...payload.game };

    // Turn result compuation - core game logic
    case GAME_ACTIONS.PLAYER_TURN:
      return makePlayerTurn(state, payload);

    default:
      return state;
  }
}
