/**
 * playerReducer modifyes Ngrx store of player data
 */

import { Action } from '@ngrx/store';

import { PLAYER_ACTIONS } from './../actions/playerActions';
import { IGame } from './../interfaces/IGame';
import { IPlayer } from './../interfaces/IPlayer';
import { pickPlayer } from './../services/gameHelpers';

export function playerReducer(
  state: IPlayer = {
    order: null,
    score: null,
    username: null,
  },
  action: Action,
): IPlayer {
  const { type, payload } = action;

  switch (type) {
    case PLAYER_ACTIONS.CREATE:
      return { ...payload.player };

    case PLAYER_ACTIONS.SYNC_PLAYER_SCORE: {
      const player: IPlayer = payload.player;
      const game: IGame = payload.game;
      const playerData: IPlayer = pickPlayer(game, player.username);
      return {
        ...player,
        score: playerData.score,
      };
    }

    default:
      return state;
  }
}
