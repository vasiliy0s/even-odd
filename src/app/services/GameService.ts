/**
 * GameService is service for game backend connection
 */

import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { Observable, Subscription } from 'rxjs';

import {
  allowCreateGame,
  allowJoinGame,
  createGame,
  gameExit,
  joinGame,
  playerTurn,
  updateGame,
} from '../actions/gameActions';

import {
  createPlayer,
  syncPlayerScore,
} from '../actions/playerActions';

import {
  isGameJoinable,
  pickPlayer,
} from './gameHelpers';

import { IGame } from '../interfaces/IGame';
import { IPlayer } from '../interfaces/IPlayer';
import { ITurn } from './../interfaces/ITurn';

@Injectable()
export class GameService {
  private game: Observable<IGame>;
  private player: Observable<IPlayer>;
  private lastGamesListSubscription: Subscription;

  constructor(
    private db: AngularFireDatabase,
    private store: Store<{}>,
  ) {
    this.game = store.select('game');
    this.player = store.select('player');
  }

  // Check available games
  public checkGame(): void {
    const query: {[ k: string ]: number } = { limitToLast: 1 };

    this.lastGamesListSubscription = this.db.list('/games', { query })
      .subscribe((games: IGame[]) => {
        const firstGame: IGame = games[0];

        if (firstGame && isGameJoinable(firstGame)) {
          this.store.dispatch(
            allowJoinGame({ ...firstGame, id: firstGame.$key }),
          );
        } else {
          this.store.dispatch(allowCreateGame());
        }
      });
  }

  // Create new game and watch for changes
  public createGame(maxPlayers: number, creatorUsername: string): void {
    this.store.dispatch(createGame(maxPlayers, creatorUsername));

    this.game.first().subscribe((game: IGame) => {
      this.disposeLastGamesListSubscription();

      const player: IPlayer = pickPlayer(game, creatorUsername);
      this.store.dispatch(createPlayer(player));
    });
  }

  // Hook as successfull op callback.
  public onGameCreated(): void {
    this.listenGameChanges();
  }

  // Join current game and watch for changes
  public joinGame(username: string): void {
    this.store.dispatch(joinGame(username));

    this.game.first().subscribe((game: IGame) => {
      this.disposeLastGamesListSubscription();

      const player: IPlayer = pickPlayer(game, username);
      this.store.dispatch(createPlayer(player));
    });
  }

  public gameExit(): void {
    this.store.dispatch(gameExit());
  }

  // Hook as successful op callback.
  public onGameJoined(): void {
    this.listenGameChanges();
  }

  // Stop listen game changes and wait for new available games or allow to create.
  public onGameExit(): void {
    this.disposeCurrentGameListen();
    this.checkGame();
  }

  // Make turn in current round from player
  public playerTurn(turn: ITurn, player: IPlayer): void {
    this.store.dispatch(playerTurn(turn, player));
  }

  public afterPlayerTurn(game: IGame, player: IPlayer): void {
    this.store.dispatch(syncPlayerScore(game, player));
  }

  // Listen current game changes on server.
  private listenGameChanges(): void {
    this.game.first().subscribe(({ id }: IGame) => {
      const gameRef: firebase.database.Reference =
        this.db.list('/games').$ref.ref.child(id);

      // tslint:disable no-any
      const callback: any = gameRef
      // tslint:enable no-any
        .on('value', (snapshot: firebase.database.DataSnapshot) => {
          this.store.dispatch(updateGame(snapshot.val()));
        });

      this.disposeCurrentGameListen = (): void => {
        gameRef.off('value', callback);
        delete this.disposeCurrentGameListen;
      };
    });
  }

  // This method should be overwritten with real callback
  // (sorry, it's Firebase events architecture!) in #listenGameChanges();
  private disposeCurrentGameListen(): void {
    // @see #listenGameChanges()
  }

  // Dispose available games subscribtions
  private disposeLastGamesListSubscription(): void {
    if (this.lastGamesListSubscription) {
      this.lastGamesListSubscription.unsubscribe();
      this.lastGamesListSubscription = null;
    }
  }
}
