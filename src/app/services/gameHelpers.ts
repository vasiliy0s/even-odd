/**
 * GameHelpers presense library functions to manage IGame during
 * gameplay process.
 */

import { IGame, JOINABLE_GAME_STATUSES } from '../interfaces/IGame';
import { IPlayer } from '../interfaces/IPlayer';
import { ITable } from './../interfaces/ITable';
import { ITurn } from './../interfaces/ITurn';

  // Pick player by username
export function pickPlayer(game: IGame, username: string): IPlayer {
  return game.players.find(
    comparePlayersUsername.bind(null, username),
  );
}

// Compare players by username
export function comparePlayersUsername(dstPlayerName: string, srcPlayer: IPlayer): boolean {
  return srcPlayer.username === dstPlayerName;
}

// Get next player turn.
export function getNextPlayerTurn(round: ITurn[]): ITurn {
  return round.find((tt: ITurn) => !tt.complete);
}

// Check if next turn is of player:
export function isPlayerTurn(dstPlayerName: string, round: ITurn[]): boolean {
  const nextPlayerTurn: ITurn = getNextPlayerTurn(round);

  return nextPlayerTurn && nextPlayerTurn.username === dstPlayerName;
}

// Check for round is complete.
export function isRoundComplete(round: ITurn[]): boolean {
  const nextPlayerTurn: ITurn = round.find((tt: ITurn) => !tt.complete);

  return !nextPlayerTurn;
}

export function previousTurnExists(rounds: ITurn[][], dstPlayerName: string): boolean {
  if (rounds.length > 1) {
    return true;
  }
  const initialTurn: ITurn = rounds[0][0];

  return initialTurn && initialTurn.username !== dstPlayerName;
}

export function getInitialCreatorTurn(game: IGame): ITurn {
  const { rounds } = game;
  return rounds.length ? rounds[0][0] : null;
}

export function samePlayerTurn(turnA: ITurn, turnB: ITurn): boolean {
  return turnA.username === turnB.username;
}

// We need to detect if initial turn made by current player
// then we return rounds.length - 1 for game round number
// because initial player (aka game creator) only made number
// in first turn, but not give answer and have no possibility
// to earn score
export function getGameRound(game: IGame): number {
  const roundsNumber = game.rounds.length;
  const initialCreatorTurn = getInitialCreatorTurn(game);
  const nextTurn = getNextPlayerTurn(game.rounds.slice(-1)[0]);

  return samePlayerTurn(initialCreatorTurn, nextTurn) ?
          (roundsNumber - 1) :
          roundsNumber;
}

// export function getPlayerRoundTurn(player: IPlayer, round: ITurn[]): ITurn {
//   return round.find(
//     (turn: ITurn): boolean => comparePlayersUsername(turn.username, player),
//   ) || null;
// }

export function getPreviousRoundTurn(rounds: ITurn[][]): ITurn {
  const currentRound: ITurn[] = rounds.slice(-1)[0];
  const currentTurn: ITurn = currentRound.find((tt: ITurn): boolean => !tt.complete);
  const currentTurnIndex: number = currentRound.indexOf(currentTurn);

  let prevTurn: ITurn = null;
  if (currentTurnIndex === 0) {
    // pick last turn from prev round
    const prevRound: ITurn[] = rounds.slice(-2)[0] || null;
    if (prevRound !== null) {
      prevTurn = prevRound.slice(-1)[0];
    }
  } else if (currentTurnIndex === -1) {
    // pick last turn from current round
    prevTurn = currentRound.slice(-1)[0];
  } else {
    // pick prev turn from current round
    prevTurn = currentRound[currentTurnIndex - 1];
  }

  return prevTurn;
}

export function sortPlayersByScore(playerA: IPlayer, playerB: IPlayer): number {
  return playerB.score - playerA.score;
}

// Check is player made even number
export function isEvenTurn(turn: ITurn): boolean {
  return turn.madeNumber % 2 === 0;
}

// Check for current turn is right
export function isRightTurnAnswer(prevTurn: ITurn, turn: ITurn): boolean {
  return isEvenTurn(prevTurn) === turn.answerIsEven;
}

// Check if player can join to game.
export function isGameJoinable(game: IGame): boolean {
  return JOINABLE_GAME_STATUSES.indexOf(game.status) >= 0 &&
          Boolean(game.status) &&
            game.players.length < game.maxPlayers;
}

// Create round turns queue for players.
export function makeRoundTurnsQueueFor(players: IPlayer[]): ITurn[] {
  // Keep order of players as order for turns
  return players.reduce(
    (turns: ITurn[], player: IPlayer): ITurn[] => turns.concat({
      answerIsEven: null,
      complete: false,
      madeNumber: null,
      result: null,
      username: player.username,
    }),
    [],
  );
}

// Get game winner by score from players list
export function getWinner(players: IPlayer[]): IPlayer {
  return players.reduce(
    (table: ITable, player: IPlayer) => {
      if (player.score <= 0) {
        return table;
      } else if (player.score > table.score) {
        return { winner: player, score: player.score, count: 1 };
      } else if (player.score === table.score) {
        return { winner: null, count: table.count + 1, score: table.score };
      }

      return table;
    },
    { winner: null, score: -1, count: 0 },
  )
  .winner || null;
}

// Add next round for players and return game.rounds
export function addNextRound(rounds: ITurn[][], players: IPlayer[]): ITurn[][] {
  return rounds.concat(
    [
      makeRoundTurnsQueueFor(players),
    ],
  );
}
