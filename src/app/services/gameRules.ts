/**
 * gameRules is pure functions collection to process certain game logic
 */

import {
  addNextRound,
  getWinner,
  isRightTurnAnswer,
  makeRoundTurnsQueueFor,
} from '../services/gameHelpers';

import { GAME_STATUSES, IGame } from './../interfaces/IGame';
import { IPlayer } from './../interfaces/IPlayer';
import { ITurn } from './../interfaces/ITurn';

// Score per good answer
export const MADE_EVEN_SCORE: number = parseInt(process.env['GAME.MADE_EVEN_SCORE'], 10) || 1;

// Maximal users rounds
export const MAX_ROUNDS: number = parseInt(process.env['GAME.MAX_ROUNDS'], 10) || 10;

export const GAME_RULES_ERRORS: {[key: string]: string} = {
  BAD_GAME_STATUS     : 'BAD_GAME_STATUS',
  MAX_PLAYERS_OVERHEAD: 'MAX_PLAYERS_OVERHEAD',
};

// New created game scaffold
const newGame: IGame = {
  id: null,
  maxPlayers: 2,
  players: [],
  rounds: [],
  status: GAME_STATUSES.GAME_AWAIT,
  winner: null,
};

export interface IPlayerTurnPayload {
  player: IPlayer;
  turn: ITurn;
}

// makePlayerTurn is pure function which generates new game state
// according to player' turn
export function makePlayerTurn(game: IGame, payload: IPlayerTurnPayload): IGame {
  const { rounds } = game;
  const { player, turn } = payload;
  const prevRounds: ITurn[][] = rounds.length > 1 ? rounds.slice(0, -1) : [];

  // Will detect this vars during the turn computation.
  let winner: IPlayer = null;
  let { status } = game;

  let prevTurn: ITurn = null;
  let isPlayerMadeEven: boolean = false;
  let isLastRoundTurn: boolean = false;
  const currentRound: ITurn[] = rounds.slice(-1)[0]
    // Finding current player' turn to compute non-mutated results
    .map((tt: ITurn, index: number, round: ITurn[]): ITurn => {
      if (tt.username !== player.username) {
        prevTurn = tt;

        return tt;
      } else {
        const turnResult: boolean = Boolean(prevTurn) ?
            isRightTurnAnswer(prevTurn, turn) :
            (prevRounds.length > 0 ?
              isRightTurnAnswer(prevRounds.slice(-1)[0].slice(-1)[0], turn) :
              null
            );
        isPlayerMadeEven = turnResult;
        isLastRoundTurn = index === round.length - 1;

        return {
          answerIsEven: turn.answerIsEven,
          complete: true,
          madeNumber: turn.madeNumber,
          result: turnResult,
          username: player.username,
        };
      }
    });

  const players: IPlayer[] = [...game.players];
  if (isPlayerMadeEven) {
    const gamePlayerIndex: number = players.findIndex(
      (pp: IPlayer) => pp.username === player.username,
    );
    let score: number = players[gamePlayerIndex].score;
    if (isNaN(score)) {
      score = 0;
    }
    players[gamePlayerIndex] = {
      ...players[gamePlayerIndex],
      score: score + MADE_EVEN_SCORE,
    };
  }

  // Turns are ended and players playing before first give
  // highest score than opponents
  let nextTurnRounds: ITurn[][] = prevRounds.concat([currentRound]);
  if (nextTurnRounds.length > MAX_ROUNDS) {
    // GAME_END detected! Now trying to select the winner...
    winner = getWinner(players);
    if (winner !== null) {
      status = GAME_STATUSES.GAME_ENDED;
    }
  } else if (isLastRoundTurn) {
    nextTurnRounds = addNextRound(nextTurnRounds, players);
  }

  return { ...game, players, rounds: nextTurnRounds, status, winner };
}

// Create new game
export function createGame(maxPlayers: number, username: string): IGame {
  const players: IPlayer[] = [{
    order: 1,
    score: 0,
    username,
  }];

  return { ...newGame, players, maxPlayers };
}

export function joinGame(game: IGame, username: string): IGame {
  if (game.players.length >= game.maxPlayers) {
    throw new Error(GAME_RULES_ERRORS.MAX_PLAYERS_OVERHEAD);
  } else if (game.status !== GAME_STATUSES.GAME_AWAIT) {
    throw new Error(GAME_RULES_ERRORS.BAD_GAME_STATUS);
  }

  const players: IPlayer[] = Array.prototype.concat.call(game.players, {
    order: game.players.length,
    score: 0,
    username,
  });

  const startGame: boolean = players.length === game.maxPlayers;

  // Detect game start
  const status = startGame ? GAME_STATUSES.GAME_STARTED : GAME_STATUSES.GAME_AWAIT;
  const rounds = startGame ? [makeRoundTurnsQueueFor(players)] : [];

  return { ...game, players, rounds, status };
}
