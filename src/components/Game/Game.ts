/**
 * Certain game component
 */

import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Observer } from 'rxjs';

import { Unsubscribable } from '../Unsubscribable/Unsubscribable';
import { IGame } from './../../app/interfaces/IGame';
import { IPlayer } from './../../app/interfaces/IPlayer';
import { ITurn } from './../../app/interfaces/ITurn';
import { MADE_EVEN_SCORE } from './../../app/services/gameRules';
import { GameService } from './../../app/services/GameService';

import {
  getGameRound,
  getPreviousRoundTurn,
  isPlayerTurn,
  previousTurnExists,
} from './../../app/services/gameHelpers';

@Component({
  selector: 'even-odd-game',
  templateUrl: 'Game.html',
})
export class Game extends Unsubscribable {
  public players: Observable<IPlayer[]>;
  public game: Observable<IGame>;
  public rounds: Observable<ITurn[][]>;
  public currentRound: Observable<ITurn[]>;
  public player: Observable<IPlayer>;
  public isPlayerTurn: Observable<boolean>;
  public previousTurnExists: Observable<boolean>;
  public turnCommitObserver: Observer<ITurn>;
  public roundNumber: Observable<number>;
  public username: string;
  private playerTurnTabloObserver: Observable<string>;

  constructor(
    store: Store<{}>,
    private gs: GameService,
  ) {
    super();

    this.game = store.select('game');
    this.player = store.select('player');

    this.player
      .takeUntil(this.ngUnsubscribe)
      .subscribe(({ username }: IPlayer) => this.username = username);

    this.players = new Observable((observer: Observer<IPlayer[]>): void => {
      this.game
        .takeUntil(this.ngUnsubscribe)
        .subscribe(({ players }: IGame) => observer.next(players));
    });

    // Watch for game turns
    this.rounds = new Observable((observer: Observer<ITurn[][]>): void => {
      this.game
        .takeUntil(this.ngUnsubscribe)
        // Bypass game ending when current game is resetted to defaults
        // with empty rounds:
        .filter(({ rounds }: IGame): boolean => rounds.length > 0)
        // ..then pass to subscribers
        .subscribe(({ rounds }: IGame) => observer.next(rounds));
    });

    // Notify about turn result after changes
    this.playerTurnTabloObserver = new Observable<string>((subscriber) => {
      this.rounds
        .takeUntil(this.ngUnsubscribe)
        .map(rounds => getPreviousRoundTurn(rounds))
        .distinctUntilKeyChanged('username')
        .skip(2) // skip initial state and first turn
        .subscribe(() => {
          this.rounds.first().subscribe((rounds) => {
            const message = this.getTurnResultMessage(rounds);
            subscriber.next(message);
          });
        });
    });

    // Wathc for current turn
    this.currentRound = new Observable((observer: Observer<ITurn[]>): void => {
      this.rounds
        .takeUntil(this.ngUnsubscribe)
        .subscribe((rounds: ITurn[][]) => {
          const currentRound: ITurn[] = rounds.slice(-1)[0];
          observer.next(currentRound);
        });
    });

    this.previousTurnExists = new Observable((observer: Observer<boolean>): void => {
      this.rounds
        .takeUntil(this.ngUnsubscribe)
        .subscribe((rounds: ITurn[][]) =>
          observer.next(previousTurnExists(rounds, this.username)),
        );
    });

    this.isPlayerTurn = new Observable((observer: Observer<boolean>): void => {
      this.currentRound
        .takeUntil(this.ngUnsubscribe)
        .subscribe((currentRound: ITurn[]) => {
          observer.next(isPlayerTurn(this.username, currentRound));
        });
    });

    this.roundNumber = new Observable<number>((observer: Observer<number>): void => {
      this.game
        .takeUntil(this.ngUnsubscribe)
        .subscribe(game => observer.next(getGameRound(game)));
    });

    const turnCommitObservable: Observable<ITurn> =
      new Observable<ITurn>((observer: Observer<ITurn>): void => {
        this.turnCommitObserver = observer;
      });

    turnCommitObservable
      .takeUntil(this.ngUnsubscribe)
      .subscribe(this.onPlayerTurn.bind(this));
  }

  public onPlayerTurn(turn: ITurn): void {
    this.player.first().subscribe((player: IPlayer): void => {
      this.gs.playerTurn(turn, player);
    });
  }

  private getTurnResultMessage(rounds: ITurn[][]): string {
    const prevPlayerTurn: ITurn = getPreviousRoundTurn(rounds);
    const username: string = prevPlayerTurn.username;
    const result: boolean = prevPlayerTurn.result;

    return result ?
      `◕‿◕: ${username} made RIGHT answer and earn ${MADE_EVEN_SCORE} score` :
      `⊙︿⊙: ${username} made BROKEN answer`;
  }
}
