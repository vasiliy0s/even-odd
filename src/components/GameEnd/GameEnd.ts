/**
 * GameEnd show game results and 'exit' button
 */

import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { NavController } from 'ionic-angular';
import { Observable, Subscriber } from 'rxjs';

import { IGame } from '../../app/interfaces/IGame';
import { IPlayer } from '../../app/interfaces/IPlayer';
import { Unsubscribable } from '../Unsubscribable/Unsubscribable';
import { GameService } from './../../app/services/GameService';
import { StartPage } from './../../pages/StartPage/StartPage';

import {
  comparePlayersUsername,
  sortPlayersByScore,
} from '../../app/services/gameHelpers';

@Component({
  selector: 'even-odd-game-end',
  templateUrl: 'GameEnd.html',
})
export class GameEnd extends Unsubscribable {
  public sortedPlayers: Observable<IPlayer[]>;
  public player: Observable<IPlayer>;
  public playerScore: Observable<number>;
  public playerIsWon: Observable<boolean>;
  private game: Observable<IGame>;

  constructor(
    private navCtrl: NavController,
    private gs: GameService,
    store: Store<any>,
  ) {
    super();

    this.game = store.select('game');

    this.player = store.select('player');

    this.sortedPlayers = new Observable<IPlayer[]>((observable: Subscriber<IPlayer[]>): void => {
      this.game
        .takeUntil(this.ngUnsubscribe)
        .subscribe((gg: IGame): void => {
          const sortedPlayers: IPlayer[] = gg.players.slice()
            .sort(sortPlayersByScore);
          observable.next(sortedPlayers);
        });
    });

    this.playerScore = new Observable<number>((observable: Subscriber<number>): void => {
      this.player
        .takeUntil(this.ngUnsubscribe)
        .subscribe((player: IPlayer): void => {
          observable.next(player.score);
        });
    });

    this.playerIsWon = new Observable<boolean>((observable: Subscriber<boolean>): void => {
      this.player
        .takeUntil(this.ngUnsubscribe)
        .subscribe((player: IPlayer): void => {
          this.game.first().subscribe((gg: IGame): void => {
            const winner: IPlayer = gg.winner;
            observable.next(
              comparePlayersUsername(winner.username, player),
            );
          });
        });
    });
  }

  public exitGame(): void {
    this.navCtrl.push(StartPage);
    this.gs.gameExit();
  }
}
