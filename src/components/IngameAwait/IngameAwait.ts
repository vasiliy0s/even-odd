/**
 * IngameAwait component show game status during other players
 * are coming to tame
 */

import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subscriber } from 'rxjs';

import { Unsubscribable } from '../Unsubscribable/Unsubscribable';
import { IGame } from './../../app/interfaces/IGame';
import { IPlayer } from './../../app/interfaces/IPlayer';

@Component({
  selector: 'even-odd-ingame-await',
  templateUrl: 'IngameAwait.html',
})
export class IngameAwait extends Unsubscribable {
  public players: Observable<IPlayer[]>;
  public player: Observable<IPlayer>;
  public maxPlayers: Observable<number>;
  public username: string;

  constructor(
    store: Store<IGame>,
  ) {
    super();

    // Provide player to players board
    this.player = store.select('player');

    const game: Observable<IGame> = store.select('game');

    this.players = new Observable<IPlayer[]>((observable: Subscriber<IPlayer[]>): void => {
      game
        .takeUntil(this.ngUnsubscribe)
        .subscribe(({ players }: IGame) => observable.next(players));
    });

    this.maxPlayers = new Observable<number>((observable: Subscriber<number>): void => {
      game
        .takeUntil(this.ngUnsubscribe)
        .subscribe(({ maxPlayers }: IGame) => observable.next(maxPlayers));
    });

    // Cache current player username;
    store.select('player')
      .takeUntil(this.ngUnsubscribe)
      .subscribe((player: IPlayer) => this.username = player.username);
  }
}
