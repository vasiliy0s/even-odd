/**
 * PlayerTurn component display form for player own turn
 */

import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Observer } from 'rxjs';

import { rangeValidator } from '../../lib/rangeValidator';
import { ITurn } from './../../app/interfaces/ITurn';

const MAX_NUMBER: number = Number.MAX_SAFE_INTEGER;

@Component({
  selector: 'even-odd-player-turn',
  templateUrl: 'PlayerTurn.html',
})
export class PlayerTurn implements OnInit {
  @Input() public turnCommitObserver: Observer<ITurn>;
  @Input() public previousTurnExists: Observable<boolean>;
  @Input() public username: string;
  public readonly MAX_NUMBER: number = MAX_NUMBER;
  public turnForm: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) {
    //
  }

  public ngOnInit(): void {
    // Create form with validation
    this.turnForm = this.fb.group({
      answerIsEven: [null, null, this.requireIfPreviousTurnExists.bind(this)],
      madeNumber: [null, Validators.compose([
        Validators.required,
        rangeValidator(1, MAX_NUMBER),
      ])],
    });
  }

  // Commit players' turn
  public commitTurn(): void {
    const turn: ITurn = {
      answerIsEven: this.turnForm.value.answerIsEven === 'true',
      complete: true,
      madeNumber: this.turnForm.value.madeNumber,
      result: null,
      username: this.username,
    };

    this.turnCommitObserver.next(turn);
  }

  private requireIfPreviousTurnExists (c: AbstractControl): Observable<{ [key: string]: boolean }> {
    const value: boolean = c.value === 'true' || c.value === 'false';

    return new Observable((observer: Observer<{ [key: string]: boolean }>): void => {
      this.previousTurnExists.first().subscribe((previousTurnExists: boolean) => {
        const req: {} = (previousTurnExists && !value) ?
                      { required: true } :
                      null;
        observer.next(req);
        observer.complete();
      });
    });
  }
}
