/**
 * PlayersBoard is component which show all game players
 */
import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Unsubscribable } from '../Unsubscribable/Unsubscribable';
import { IPlayer } from './../../app/interfaces/IPlayer';
import { ITurn } from './../../app/interfaces/ITurn';

import {
  comparePlayersUsername,
  isPlayerTurn,
} from './../../app/services/gameHelpers';

@Component({
  selector: 'even-odd-players-board',
  styleUrls: ['PlayersBoard.scss'],
  templateUrl: 'PlayersBoard.html',
})
export class PlayersBoard extends Unsubscribable implements OnInit {
  @Input() public players: Observable<IPlayer[]>;
  @Input() public player: Observable<IPlayer>;
  @Input() public round: Observable<ITurn[]>;
  @Input() public simpleMode: boolean;

  private roundList: ITurn[] = [];
  private username: string;

  public isCurrentPlayer(player: IPlayer): boolean {
    return comparePlayersUsername(this.username, player);
  }

  public isCurrentPlayerTurn(player: IPlayer): boolean {
    const { roundList } = this;

    return roundList.length > 0 && isPlayerTurn(player.username, roundList);
  }

  public ngOnInit(): void {
    // Handle current player username to get highlight on board
    const { player, round } = this;

    if (player) {
      player
        .takeUntil(this.ngUnsubscribe)
        .subscribe(({ username }: IPlayer) => this.username = username);
    }

    if (round) {
      round
        .takeUntil(this.ngUnsubscribe)
        .subscribe((roundList: ITurn[]) => this.roundList = roundList);
    }
  }
}
