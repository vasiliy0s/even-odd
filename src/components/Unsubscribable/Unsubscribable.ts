/**
 * Auto-unsubscrible component Unsubscribable
 */

import { OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

export class Unsubscribable implements OnDestroy {
  protected ngUnsubscribe: Subject<any>  = new Subject();

  public ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
