/**
 *  This module read Firebase configuration
 * from .env file in root folder using dotenv module
 * (need to be prerequired with following command:
 *  `node -r dotenv/config ./node_modules/.bin/ionic-app-scripts [command] [options]`)
 * and returns as set of
 */

import dotenv from 'dotenv';

dotenv.config();

export const FIREBASE_CONFIG: {} = {
  apiKey           : process.env['FIREBASE.apiKey'],
  authDomain       : process.env['FIREBASE.authDomain'],
  databaseURL      : process.env['FIREBASE.databaseURL'],
  messagingSenderId: process.env['FIREBASE.messagingSenderId'],
  projectId        : process.env['FIREBASE.projectId'],
  storageBucket    : process.env['FIREBASE.storageBucket'],
};
