/**
 * rangeValidator is simple custom validator
 * which allow input number between [min, max] value
 */

import { AbstractControl, ValidatorFn } from '@angular/forms';

export function rangeValidator(min: number, max: number): ValidatorFn {
  return (control: AbstractControl): {[key: string]: {}} => {
    const { value } = control;

    if (value > max) {
      return { maxValue: { max } };
    } else if (value < min) {
      return { minValue: { min } };
    }

    return null;
  };
}
