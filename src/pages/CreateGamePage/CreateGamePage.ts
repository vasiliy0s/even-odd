/**
 * CreateGamePage component provide form for new game creation
 */

import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireDatabase } from 'angularfire2/database';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs';
import { rangeValidator } from '../../lib/rangeValidator';
import { IngamePage } from '../IngamePage/IngamePage';

import { GameService } from './../../app/services/GameService';

const MAX_PLAYERS: number = 10;

@Component({
  selector: 'page-create-game',
  templateUrl: 'CreateGamePage.html',
})
export class CreateGamePage {
  public createGameForm: FormGroup;
  public readonly MAX_PLAYERS: number = MAX_PLAYERS;

  constructor(
    private navCtrl: NavController,
    fb: FormBuilder,
    private gs: GameService,
  ) {
    this.createGameForm = fb.group({
      maxPlayers: [null, rangeValidator(2, this.MAX_PLAYERS)],
      username: [null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(20),
      ])],
    });
  }

  public back(): void {
    this.navCtrl.pop();
  }

  private createGame(): void {
    const { maxPlayers, username } = this.createGameForm.value;
    this.gs.createGame(parseInt(maxPlayers, 10), username);
    this.navCtrl.push(IngamePage);
  }
}
