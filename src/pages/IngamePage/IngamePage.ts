/**
 * IngamePage component show 'game' screen
 */

import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subscriber } from 'rxjs';

import { GAME_STATUSES, IGame } from './../../app/interfaces/IGame';
import { IPlayer } from './../../app/interfaces/IPlayer';
import { Unsubscribable } from './../../components/Unsubscribable/Unsubscribable';

@Component({
  selector: 'page-ingame',
  templateUrl: 'IngamePage.html',
})
export class IngamePage extends Unsubscribable {
  public readonly GAME_STATUSES: object = GAME_STATUSES;
  public gameStatus: Observable<string>;
  public player: Observable<IPlayer>;

  constructor(
    public store: Store<IGame>,
  ) {
    super();

    this.player = store.select('player');
    this.gameStatus = new Observable<string>((observable: Subscriber<string>): void => {
      store.select('game')
        .takeUntil(this.ngUnsubscribe)
        .subscribe(({ status }: IGame) => observable.next(status));
    });
  }
}
