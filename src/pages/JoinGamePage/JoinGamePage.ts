/**
 * JoinGamePage is a screen component which allows
 * players to join to exists game
 */
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs';

import { IngamePage } from '../IngamePage/IngamePage';
import { IGame } from './../../app/interfaces/IGame';
import { GameService } from './../../app/services/GameService';
import { Unsubscribable } from './../../components/Unsubscribable/Unsubscribable';

@Component({
  selector: 'page-join-game',
  templateUrl: 'JoinGamePage.html',
})
export class JoinGamePage extends Unsubscribable implements OnInit {
  public joinGameForm: FormGroup;
  public forbiddenUsernames: Observable<string[]>;

  constructor(
    public navCtrl: NavController,
    private gs: GameService,
    private fb: FormBuilder,
    store: Store<any>,
  ) {
    super();

    this.forbiddenUsernames = new Observable((observer) => {
      store.select('game')
        .takeUntil(this.ngUnsubscribe)
        .subscribe((game: IGame) => {
          observer.next(game.players.map(p => p.username));
        });
    });
  }

  public ngOnInit() {
    this.joinGameForm = this.fb.group({
      username: [null, null, this.checkForbiddenUsernames.bind(this)],
    });
  }

  public joinGame(): void {
    const username: string = this.joinGameForm.value.username;
    this.gs.joinGame(username);
    this.navCtrl.push(IngamePage);
  }

  public back(): void {
    this.navCtrl.pop();
  }

  // Signal about forbidden usernames
  private checkForbiddenUsernames(c: AbstractControl) {
    const username = c.value;

    return new Observable((observer) => {
      this.forbiddenUsernames.first().subscribe((forbiddenUsernames) => {
        const res = forbiddenUsernames.indexOf(username) >= 0 ?
                    { forbiddenUsername: true } :
                    null;
        observer.next(res);
        observer.complete();
      });
    });
  }
}
