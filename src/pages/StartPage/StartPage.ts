/**
 * StartPage is initial page for app
 */

import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { NavController } from 'ionic-angular';
import { Observable, Subscriber } from 'rxjs';

import { CreateGamePage } from '../CreateGamePage/CreateGamePage';
import { JoinGamePage } from '../JoinGamePage/JoinGamePage';
import {
  IGame,
  JOINABLE_GAME_STATUSES,
} from './../../app/interfaces/IGame';
import { Unsubscribable } from './../../components/Unsubscribable/Unsubscribable';

const STRATEGIES: {[key: string]: string} = {
  CONNECTION_INITIALIZATION: 'CONNECTION_INITIALIZATION',
  CREATE_GAME              : 'CREATE_GAME',
  JOIN_GAME                : 'JOIN_GAME',
};

@Component({
  selector: 'page-start',
  styleUrls: ['StartPage.scss'],
  templateUrl: 'StartPage.html',
})
export class StartPage extends Unsubscribable {
  public startStrategy: Observable<string>;
  public STRATEGIES: {[key: string]: string} = STRATEGIES;

  constructor(
    public navCtrl: NavController,
    store: Store<{}>,
  ) {
    super();

    this.startStrategy = new Observable<string>((observable: Subscriber<string>): void  => {
      // Default and initial start page is game creation
      observable.next(STRATEGIES.CONNECTION_INITIALIZATION);

      // Watch for game status to show 'create' or 'join'
      // buttons accordingly.
      store.select('game')
        .skip(1)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(({ status }: IGame) => {
          if (this.isJoinableGameStatus(status)) {
            observable.next(STRATEGIES.JOIN_GAME);
          } else {
            observable.next(STRATEGIES.CREATE_GAME);
          }
        });
    });
  }

  public createGame(): void {
    this.navCtrl.push(CreateGamePage);
  }

  public joinGame(): void {
    this.navCtrl.push(JoinGamePage);
  }

  private isJoinableGameStatus(status: string): boolean {
    return JOINABLE_GAME_STATUSES.indexOf(status) >= 0;
  }
}
