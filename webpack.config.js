"use strict";

const path = require("path");
const webpack = require("webpack");
const ionicWebpackFactory = require(process.env.IONIC_WEBPACK_FACTORY);

const ModuleConcatPlugin = require("webpack/lib/optimize/ModuleConcatenationPlugin");

const Dotenv = require("dotenv-webpack");

module.exports = {
  entry: process.env.IONIC_APP_ENTRY_POINT,
  output: {
    path: "{{BUILD}}",
    publicPath: "build/",
    filename: "[name].js",
    devtoolModuleFilenameTemplate: ionicWebpackFactory.getSourceMapperFunction(),
  },
  devtool: process.env.IONIC_SOURCE_MAP_TYPE,

  resolve: {
    extensions: [".ts", ".js", ".json"],
    modules: [path.resolve("node_modules")],
  },

  module: {
    loaders: [
      {
        test: /\.json$/,
        loader: "json-loader",
      },
      {
        test: /\.ts$/,
        loader: process.env.IONIC_WEBPACK_LOADER,
      },
      {
        test: /\.js$/,
        loader: process.env.IONIC_WEBPACK_TRANSPILE_LOADER,
      },
    ],
  },

  plugins: [
    new Dotenv({
      // path: "./.env",
      safe: false,
    }),
    ionicWebpackFactory.getIonicEnvironmentPlugin(),
    ionicWebpackFactory.getCommonChunksPlugin(),
    new ModuleConcatPlugin(),
  ],

  // Some libraries import Node modules but don't use them in the browser.
  // Tell Webpack to provide empty mocks for them so importing them works.
  node: {
    fs: "empty",
    net: "empty",
    tls: "empty",
  },
};
